package com.sisyphus.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：RemoteConfigServerApplication
 * 类描述：RemoteConfigServerApplication
 * 创建时间：2019/10/15
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
@SpringBootApplication
@EnableConfigServer
public class RemoteConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RemoteConfigServerApplication.class, args);
    }

}

package com.sisyphus.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：GatewayApplication
 * 类描述：GatewayApplication
 * 创建时间：2019/10/14
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
@EnableZuulProxy
@EnableAutoConfiguration
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

}

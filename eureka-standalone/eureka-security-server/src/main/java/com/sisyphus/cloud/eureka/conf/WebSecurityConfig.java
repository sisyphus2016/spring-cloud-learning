package com.sisyphus.cloud.eureka.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：WebSecurityConfig
 * 类描述：WebSecurityConfig
 * 创建时间：2020/4/9
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable(); //关闭csrf
        super.configure(http); //开启认证
    }


}

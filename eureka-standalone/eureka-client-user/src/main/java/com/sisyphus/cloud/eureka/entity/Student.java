package com.sisyphus.cloud.eureka.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：Student
 * 类描述：Student
 * 创建时间：2019/10/14
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
@Data
@AllArgsConstructor
public class Student {

    private long id;

    private String name;

    private int age;

}

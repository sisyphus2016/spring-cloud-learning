package com.sisyphus.cloud.eureka.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：ApplicationController
 * 类描述：ApplicationController
 * 创建时间：2019/10/15
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
@RestController
@RequestMapping("/application")
public class ApplicationController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/index")
    public String index() {
        return "当前端口：" + port;
    }


}

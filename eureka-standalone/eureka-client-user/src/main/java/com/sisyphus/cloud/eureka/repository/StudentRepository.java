package com.sisyphus.cloud.eureka.repository;


import com.sisyphus.cloud.eureka.entity.Student;

import java.util.Collection;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：StudentRepository
 * 类描述：StudentRepository
 * 创建时间：2019/10/14
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
public interface StudentRepository {

    Collection<Student> findAll();

    Student findById(long id);

    void saveOrUpdate(Student student);

    void deleteById(long id);

}

package com.sisyphus.cloud.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：EurekaServerStandaloneApplication
 * 类描述：EurekaServerStandaloneApplication
 * 创建时间：2022/2/22
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaServerStandaloneApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServerStandaloneApplication.class, args);
    }

}

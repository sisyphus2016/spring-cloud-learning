package com.sisyphus.cloud.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：EurekaServerApplication
 * 类描述：EurekaServerApplication
 * 创建时间：2019/10/14
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServer3Application {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServer3Application.class, args);
    }

}

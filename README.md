# Spring Cloud Learning

### Spring CLoud 介绍
Spring Cloud 自 2016 年 1 月发布第一个 Angel.SR5 版本，到目前 2020 年 3 月发布 Hoxton.SR3 版本，已经历经了 4 年时间。这 4 年时间里，Spring Cloud 一共发布了 46 个版本，支持的组件数从 5 个增加到 21 个。Spring Cloud 在 2019 年 12 月对外宣布后续 RoadMap：

- 下一个版本 Ilford 版本是一个大版本。这个版本基于 Spring Framework 5.3 & Spring Boot 2.4，会在 2020 Q4 左右发布；
- Ilford 版本会删除处于维护模式的项目。目前处于维护模式的 Netflix 大部分项目都会被删除（spring-cloud-netflix Github 项目已经删除了这些维护模式的项目）；
- 简化 Spring Cloud 发布列车。后续 IaasS 厂商对应的 Spring Cloud 项目会移出 Spring Cloud 组织，各自单独维护（spring-cloud-azure 一直都是单独维护，spring-cloud-alibaba 孵化在 Spring Cloud 组织，毕业后单独维护）；
- API 重构，会带来重大的改变（Spring Cloud Hoxton 版本新增了 Spring Cloud Circuit Breaker 用于统一熔断操作的编程模型和 Spring Cloud LoadBalanacer 用于处理客户端负载均衡并代替 Netflix Ribbon）。

这个 RoadMap 可以说是对 Spring Cloud 有着非常大的变化。

##### 官方网站
https://spring.io/cloud

### 项目介绍
#### 模块清单
- eureka
    - Spring Cloud Netflix Eureka 服务注册与发现框架
    - [模块介绍](./eureka/README.md)
- zuul-gateway 网关
- service-provider 业务服务
- ribbon-client 服务调用
- feign-client 服务调用
- hystrix-client 服务调用
- native-config-server 本地配置中心服务端
- native-config-client 本地配置中心客户端
- remote-config-client 远程配置中心服务端
- remote-config-server 远程配置中心客户端
- zipkin 服务跟踪
- zipkin-client


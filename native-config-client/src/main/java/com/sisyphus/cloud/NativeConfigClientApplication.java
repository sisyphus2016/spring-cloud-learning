package com.sisyphus.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：NativeConfigClientApplication
 * 类描述：NativeConfigClientApplication
 * 创建时间：2019/10/15
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
@SpringBootApplication
public class NativeConfigClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(NativeConfigClientApplication.class, args);
    }

}

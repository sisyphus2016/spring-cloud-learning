package com.sisyphus.cloud.feign;

import com.sisyphus.cloud.entity.Student;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collection;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：FeignProviderClient
 * 类描述：FeignProviderClient
 * 创建时间：2019/10/15
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
@FeignClient(value = "USER-SERVICE", fallback = FeignError.class)
public interface FeignProviderClient {

    @GetMapping("/student/findAll")
    Collection<Student> findAll();

    @GetMapping("/index")
    String index();

}

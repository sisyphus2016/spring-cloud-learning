package com.sisyphus.cloud.feign;

import com.sisyphus.cloud.entity.Student;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：FeignError
 * 类描述：FeignError
 * 创建时间：2019/10/15
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
@Component
public class FeignError implements FeignProviderClient {

    @Override
    public Collection<Student> findAll() {
        return null;
    }

    @Override
    public String index() {
        return "服务暂不可用，稍后再试";
    }

}

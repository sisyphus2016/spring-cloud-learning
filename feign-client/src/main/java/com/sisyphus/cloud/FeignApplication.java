package com.sisyphus.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 项目名称：spring-cloud-learning
 * 类名称：FeignApplication
 * 类描述：FeignApplication
 * 创建时间：2019/10/15
 *
 * @author sisyphus   (E-mail:1620657419@qq.com)
 * @version v1.0
 */
@EnableFeignClients
@SpringBootApplication
public class FeignApplication {

    public static void main(String[] args) {
        SpringApplication.run(FeignApplication.class, args);
    }

}
